/*
    Controller Services Workers
*/

(function () {
    /*
        Controller Cache
    */
    var CACHE_NAME = 'v3';
    var cacheFiles = [
        './',
        './index.html',
        './app/assets/vendor/materialize/css/materialize.css',
        './app/assets/vendor/materialize/js/materialize.min.js',
        './app.js'
    ]

    /*
        Controller listener events Services Workers
    */
    self.addEventListener('install', function (event) {
        console.log("[ServiceWorker] Installed");

        /*Add Files on Cache */
        event.waitUntil(
            caches.open(CACHE_NAME).then(function (cache) {
                console.log("[ServiceWorker] Caching cacheFiles");
                return cache.addAll(cacheFiles);
            })
        )
    });

    self.addEventListener('activate', function (event) {
        console.log("[ServiceWorker] Activated");

        /*Clearing Old Cache*/
        event.waitUntil(
            caches.keys().then(function (CACHE_NAMEs) {
                return Promise.all(CACHE_NAMEs.map(function (thisCACHE_NAME) {

                    if (thisCACHE_NAME !== CACHE_NAME) {
                        console.log("[ServiceWorer] Removing Cached Files from ", thisCACHE_NAME);
                        return caches.delete(thisCACHE_NAME);
                    }
                }))
            })
        )
    });

    self.addEventListener('fetch', function (event) {
        /*Handling Requests*/

        event.respondWith(
            caches.match(event.request)
                .then(function (response) {
                    /* Return response from the cached*/
                    if (response)
                        return response;

                    return fetch(event.request);
                })
        );

        console.log("[ServiceWorker] Fetching", event.request.url);
    });
})();